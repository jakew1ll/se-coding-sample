<?php


class player 
{
	var $name;
	var $terrainCosts = [];

	public function __construct($name, $terrain)
	{
		$this->name = $name;
		$this->terrainCosts = $terrain;
	}

	public function getCost($tileType)
	{
		return $this->terrainCosts[$tileType];
	}
}

class Map
{
	var $mapTiles = [];

	public function __construct($tilerows)
	{
		if (is_array($tilerows)) {
			foreach ($tilerows as $row) {
				if (is_array($row)) {
					$this->mapTiles[] = $row;
				} elseif (is_string($row)) {
					$this->mapTiles[] = explode($row);
				}
			}
		}
	}

	public function getStartLocation()
	{
		$location = null; 
		foreach ($mapTiles as $row_key => $tiles) {
			foreach ($tiles as $tile_key => $value) {
				if ($value == '$') {
					$location = [$row_key, $tile_key];
					break 2;
				}
			}
		}

		return $location;
	}

	public function isOneEndLocation($locations)
	{
		$return_value = false;
		foreach ($locations as $tile) {
			if ($this->getLocationValue($tile) == '@') {
				$return_value = true;
				break;
			}
		}

		return $return_value;
	}

	public function getLocationValue($location) 
	{
		return $this->mapTiles[$location[0]][$location[1]];
	}

	public function getPossibleLocations($startingLocation, $previousLocation)
	{
		$possibleLocations = [];

		// left
		if ($startingLocation[1] - 1 > 0 )
			$location = [$startingLocation[0], $startingLocation[1]-1]
			if ($this->getLocationValue($location) !== '*') {
				$possibleLocations[] = $location;
			}
		}

		//right
		if ($startingLocation[1] + 1 < count($this->mapTiles[$startingLocation[0]]) )
			$location = [$startingLocation[0], $startingLocation[1]+1]
			if ($this->getLocationValue($location) !== '*') {
				$possibleLocations[] = $location;
			}
		}

		// UP location
		if ($startingLocation[0] - 1 > 0 )
			$location = [$startingLocation[0]-1, $startingLocation[1]]
			if ($this->getLocationValue($location) !== '*') {
				$possibleLocations[] = $location;
			}
		}

		//down
		if ($startingLocation[0] + 1 < count(keys($this->mapTiles)) )
			$location = [$startingLocation[0]-1, $startingLocation[1]]
			if ($this->getLocationValue($location) !== '*') {
				$possibleLocations[] = $location;
			}
		}


		return $possibleLocations;
	}
}

class Run
{
	public $allRunCosts = [];
	public $player;
	public Map $map;
	public $currentLocation = [];
	public  

	public function __construct($player, $map) {
		$this->player=$player;
		$this->map = $map;
	}

	public function go() 
	{
		$this->allRunCosts = $this->getAllCosts();
	}

	public function getAllCosts()
	{
		$startingLocation = $this->map->getStartLocation();
		return $this->getCostOfNextLocation($startingLocation);
	}

	public function getCostOfNextLocation($startingLocation)
	{
		$locations = $this->map->getPossibleLocations($startingLocation);
		$paths = [];
		if(count($locations) > 1 || !$this->map->isOneEndLocation($locations)) {
			foreach ($locations as $key => $value) {
				$moveCost = $this->player->getCost($this->map->getLocationValue($value));
				$nextMoveCosts = $this->getCostOfNextLocation($value)
				foreach ($nextMoveCosts as $pathCost) {
					$paths[] = $pathCost + $moveCost;
				}
			}
		} 

		return $paths;
	}

	public function getBestCost()
	{
		
		$best = null;

		if ($this->allRunCosts == null || count($this->allRunCosts) < 1 ) {
			return null;
		}

		foreach ($this->allRunCosts as $cost) {
			if ($cost < $best || $best == null) {
				$best = $cost;
			}
		}
	}
}

// handle args
$options = getopt("",['filename:']);

// Main script body
// if this is a true production level tool for CLI  would use syphony or laravel CLI frameworks to take care of the templating code and make most of it OOP.
$xml_doc = simplexml_load_file($options['filename']);




// default stuff
$players = [];
$map = [];

// Get players
$players_xml = $xml_doc->xpath('/root/players/player');
foreach ($players_xml as $player)
{
	$name = (string) $player.['name'];
	$terrain = [];
	foreach ($player->children() as $tile)
	{
		$code = (string) $tile['terrain'];
		$cost = (int)(string) $tile['cost'];
		$terrain[$code] = $cost;
	}
	$players[]= new player($name,$terrain);
 }
// Get map
$mapTile = [];
$rows = $xml_doc->xpath('/root/map/row');
foreach ($rows as $row)
{
	$mapTile[] = (string) $row;
}

$map = new Map($mapTile);
// Start calculating each player
$all_runs = [];
foreach ($players as $player) {
	$run = new Run($player, $map);
	$run->go();
	$all_runs[] = $run;
}

// Get each player's best cost
$best = null;
foreach ($all_runs as $run) {
	$bestCost = $run->getBestCost();
	print_r("Player {$run->player->name} best Moves total of {$bestCost} ")
	if ($bestCost < $best->getBestCost() || $best == null) {
		$best = $run;
	}
}

// Get best player


print_r("Best Player is {$best->name} with Moves total of {$best->getBestCost()} ")

